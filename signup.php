<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="main">

        <div class="wrapper">

            <div class="showError">
                <?php
                $nameErr = $falcutyErr = $genderErr = $dobErr = $addressErr = "";
                $name = $falcuty = $gender = $dob = $address = "";

                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // Validate
                    if (empty($_POST["name"])) {
                        $nameErr = "Hãy nhập họ tên";
                        echo '<p style="color:red;">' . $nameErr . '</p>';
                    } else {
                        $name = test_input($_POST["name"]);
                    }
                    if (empty($_POST["falcuty"]) || $_POST["falcuty"] == 'None') {
                        $falcutyErr = "Hãy nhập tên khoa";
                        echo '<p style="color:red;">' . $falcutyErr . '</p>';
                    } else {
                        $falcuty = test_input($_POST["falcuty"]);
                    }

                    if (empty($_POST["gender"])) {
                        $genderErr = "Hãy nhập giới tính";
                        echo '<p style="color:red;">' . $genderErr . '</p>';
                    } else {
                        $gender = test_input($_POST["gender"]);
                    }

                    if (empty($_POST["dob"])) {
                        $dobErr = "Hãy nhập ngày sinh";
                        echo '<p style="color:red;">' . $dobErr . '</p>';
                    } else {
                        $dob = test_input($_POST["dob"]);
                    }

                    if (empty($_POST["address"])) {
                        $addressErr = "Hãy nhập địa chỉ";
                        echo '<p style="color:red;">' . $addressErr . '</p>';
                    } else {
                        $address = test_input($_POST["address"]);
                    }

                    if (empty($addressErr) && empty($dobErr) && empty($falcutyErr) && empty($dobErr) && empty($nameErr)) {
                        echo '<p style="color:#5cb85c;"> Đăng kí thành công</p>';
                    }
                }

                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                ?>
            </div>
            <form action="" method="post">
                <?php echo '<div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label></label>
                    <input class="name" type="text" name="name" id="">    
                </div>' ?>

                <div class="form-group">
                <label class="form-label">Giới tính <span class="required">*</span></label></label>
                <?php
                $gender = array("0" => "Nam", "1" => "Nữ");
                for ($i = 0; $i < count($gender); $i++) {
                    echo '<div class="radio-group">
                      <input type="radio"  name="gender" value=" ' . $i . '">
                    </div>';
                    echo '<label class="labelRadio" for="html">' . $gender[$i] . '</label>';
                }
                ?>
                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label></label>
                    <select name="falcuty" id="falcuties">
                        <?php
                            $falcuty = array("SPA" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>
                    <input id="txtDate" type="text" name="dob" placeholder="dd/mm/yyyy" style="width: 246px;">
                </div>
                <?php echo '<div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <input class="name" type="text" name="address" id="">    
                </div>' ?>

                <button class="submit-btn">Đăng ký</button>
            </form>
        </div>
    </div>
    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
</body>

</html>